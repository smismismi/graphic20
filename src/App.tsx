import React, {useCallback, useEffect, useState} from 'react';
import {Simple, SyncedCharts, ThreeD, ThreeD3, Stacked, Gauge} from 'components/HighchartsChart';
import {GraphicWrapped} from "./components/Graphic20";
import {getTagsAction} from "./store/modules/tags";
import { getRandomizedStartEnd } from "./utils";

function App() {
  const [okno, setOkno] = useState({
    start: null,
    end: null
  })
  const [tags, setTags] = useState([])
  const [selectedTags, setSelectedTags] = useState([])
  const generateRandomDateTags = useCallback(()=> {

    const wegwe = getRandomizedStartEnd();
    // @ts-ignore
    setOkno(wegwe)
    const tagsLength = 3
    const generatedTags: never[] = [];
    for (let i = 0; i < tagsLength; i++) {
      const newTag = tags[~~(Math.random() * 42)];
      if (newTag && !generatedTags.includes(newTag)) {
        generatedTags.push(newTag);
      }
    }
    // @ts-ignore
    setSelectedTags(generatedTags)
    // setSelectedTags([tags[0], tags[1]])
  }, [setOkno, setSelectedTags, tags]);

  useEffect(() => {
    const promise = async () => {
      const {tags} = await getTagsAction()
      // @ts-ignore
      setTags(tags)
    }
    promise();
  }, [])

  useEffect(() => {
    generateRandomDateTags()
  }, [tags])

  return (
    <div className="App">
      <header className="App-header">
        <button onClick={generateRandomDateTags}>generate Random Date Tags</button>
        {okno && okno.start && okno.end && tags && tags.length && <GraphicWrapped okno={okno} tags={selectedTags}/>}
        {/*<Gauge/>*/}
        {/*<Stacked/>*/}
        {/*<Simple/>*/}
        {/*<SyncedCharts/>*/}
        {/*<ThreeD/>*/}
        {/*<ThreeD3/>*/}
      </header>
    </div>
  );
}

export default App;
