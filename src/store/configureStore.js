import { createStore, compose, applyMiddleware } from 'redux';
import { rootReducer } from 'store/rootReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createHashHistory } from 'history';
export const history = createHashHistory({
  hashType: 'slash'
});

function middleware({dispatch}) {
  return next => action => {
    if (typeof action === 'function') {
      return action(dispatch)
    }

    return next(action)
  }
}

export function configureStore(initialState) {
  const ENV = 'dev'; // TODO Убрать DevTool из сборки для продакшена
  const composeEnhancer = ENV === 'dev' ? composeWithDevTools : compose;
  return  createStore(
    rootReducer(history),
    initialState,
    composeEnhancer(applyMiddleware(middleware)),
  );
}
