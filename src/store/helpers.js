import { FETCH_TIMESERIES_DATA } from './constant';

export function isModalVisible(modals, id) {
  return !!modals.visible[id];
}

export function getModalPayload(modals, id) {
  return modals.payloads[id] || {};
}

export function generateFetchDataTypes(alias) {
  return [`${alias}_SUCCESS`, `${alias}_ERROR`, `${alias}_LOADING`];
}
export function actionFetchTimeseriesData(types, promise, payload, assetId) {
  return {
    type: FETCH_TIMESERIES_DATA,
    payload: {
      ...payload,
      types,
      assetId,
      promise: () => promise
    }
  };
}
export function handlerFetchTimeseriesDataReducer(types, action, state) {
  const [success, error, loading] = types;
  const { assetId } = action.payload;
  switch (action.type) {
    case loading:
      return {
        ...state,
        loading: true,
        error: null
      };
    case error:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    case success:
      return {
        ...state,
        loading: false,
        error: null,
        data: {
          ...state.data,
          [assetId]: action.payload.data
        }
      };
    default:
      return state;
  }
}

export function handlerFetchDataReducer(types, action, state) {
  const [success, error, loading] = types;


  switch (action.type) {
    case loading:
      return {
        ...state,
        loading: true,
        error: null
      };
    case error:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    case success:
      return {
        ...state,
        loading: false,
        error: null,
        data: action.payload.data
      };
    default:
      return state;
  }
}
