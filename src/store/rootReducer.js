import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { userData } from 'store/modules/userData';
import { modals } from 'store/modules/modals';
import { casesReducer as cases } from 'store/modules/cases';
import { timeseriesReducer as timeseries } from 'store/modules/timeseries';
import { itemReducer as item } from 'store/modules/selectedItem';

export const rootReducer = () => {
  return combineReducers({
    userData,
    modals,
    item,
    cases,
    timeseries,
    form: formReducer
  });
}
