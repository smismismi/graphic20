import { CasesController } from 'api';
import reduxHelper from '../actionHelpers'

export const { actionTypes: b, action: getCaseItemAction, reducer: itemReducer } = reduxHelper('item', function(id) {
  return CasesController.getItem(id)
})



