// Constants
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

// Reducer
const initialState = {
  visible: {},
  payloads: {}
};

// Actions
export function openModal(id, payload) {
  return { type: OPEN_MODAL, id, payload };
}

export function closeModal(id) {
  return { type: CLOSE_MODAL, id };
}

// Reducer
export function modals(state = initialState, { type, id, payload }) {
  switch (type) {
    case OPEN_MODAL:
      return {
        ...state,
        visible: { ...state.visible, [id]: true },
        payloads: { ...state.payload, [id]: payload }
      };
    case CLOSE_MODAL:
      return {
        ...state,
        visible: { ...state.visible, [id]: false },
        payloads: { ...state.payload, [id]: null }
      };
    default:
      return state;
  }
}
