import { CasesController } from 'api';
import reduxHelper from '../actionHelpers'

export const { actionTypes: b, action: getCasesAction, reducer: casesReducer } = reduxHelper('cases', function() {
  return CasesController.getCases()
})



