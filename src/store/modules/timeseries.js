import {TimeseriesController} from 'api';
import reduxHelper from '../actionHelpers'

export const {actionTypes: b, action: getTimeseriesDataAction, reducer: timeseriesReducer} =
  reduxHelper(
    'timeseries',
    function (params) {
          return TimeseriesController.getHistorical(params)
        },
    (prevData, action) => {
      return action.data !== undefined ? action.data : null
    })

export const getHistoricalData = function (params) {
  return TimeseriesController.getHistorical(params)
}
