import {TagsController} from 'api';

export const getTagsAction = function () {
  return TagsController.getTags()
}

export const getUnitsAction = function (tags) {
  return TagsController.getUnits(tags)
}
