import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { configureStore } from './store/configureStore';
import { Provider } from 'react-redux';
import { ModalsContainer } from 'components/Modals';
// import { Graphic } from 'components/Graphic';
// import { Gr } from 'components/Gr';
// import { Inbox } from 'components/Inbox';

const store = configureStore({});

// const delay = (t: number, v = 0) => {
//   return new Promise(resolve => {
//     setTimeout(resolve.bind(null, v), t);
//   });
// };
// (async () => {
//   await delay(5000);

  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <App />
        <ModalsContainer />
        {/*<Inbox a={1} b={2}/>*/}
        {/*<Gr/>*/}
      </Provider>
    </React.StrictMode>,
    document.getElementById('root')
  );
// })()



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
