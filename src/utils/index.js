import { format } from "date-fns";
import { ru } from "date-fns/locale";

export const SEC = 1000;
export const MINUTE = 60 * SEC;
export const HOUR = 60 * MINUTE;
export const DAY = 24 * HOUR;
export const MONTH = 30 * DAY;

export const debounce = function(fn, time) {
  let timeout = 0;
  return function () {
    const self = this; // eslint-disable-next-line prefer-rest-params

    const args = arguments;

    const functionCall = function functionCall() {
      return fn.apply(self, args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(functionCall, time);
  };
}

export const dataShuffler = (data) => {
  Object.keys(data).forEach(tagData => {
    data[tagData] = data[tagData].map(item => [
      item[0],
      ~~(item[1] - item[1] * multiplicator[tagData])
    ])
  })
  return data;
}

export const getAggregation = (start, end) => {

  let aggregation;

  const delta = end - start;
  switch (true) {
    case (delta < SEC): {
      aggregation = 0;
      break;
    }
    case (delta < MINUTE): {
      aggregation = 0;
      break;
    }
    case (delta < HOUR): {
      aggregation = 0;
      break;
    }
    case (delta < HOUR * 3): {
      aggregation = 2;
      break;
    }
    case (delta < HOUR * 6): {
      aggregation = 4;
      break;
    }
    case (delta < HOUR * 12): {
      aggregation = 8;
      break;
    }
    case (delta < DAY): {
      aggregation = 24 / 2;
      break;
    }
    case (delta < MONTH): {
      aggregation = 24 * 12 / 4;
      break;
    }
    case (delta < MONTH * 3): {
      aggregation = 24 * 12 * 3;
      break;
    }
    case (delta < MONTH * 6): {
      aggregation = 24 * 12 * 6;
      break;
    }
    case (delta < MONTH * 12): {
      aggregation = 24 * 12 * 12;
      break;
    }
    case (delta < DAY * 365): {
      aggregation = 12 * 365;
      break;
    }
    case (delta < DAY * 365 * 2): {
      aggregation = 12 * 365  * 2;
      break;
    }
    case (delta < DAY * 365 * 4): {
      aggregation = 12 * 365  * 4;
      break;
    }
    default:
      aggregation = 12 * 365 * 6;
  }
  return aggregation
}

export const multiplicator = {
  a:    1.33,
  b:    -2,
  x00:  1.123,
  x01:  -1.01,
  x02:  1.02,
  x03:  -1.03,
  x04:  1.04,
  x05:  -1.05,
  x06:  1.06,
  x07:  -1.07,
  x08:  1.08,
  x09:  -1.09,
  x10:  1.10,
  x11:  -1.11,
  x12:  1.12,
  x13:  -1.13,
  x14:  1.14,
  x15:  -1.15,
  x16:  1.16,
  x17:  -1.17,
  x18:  1.18,
  x19:  -1.19,
  x20:  2.20,
  x21:  -2.21,
  x22:  2.22,
  x23:  -2.23,
  x24:  2.24,
  x25:  -2.25,
  x26:  2.26,
  x27:  -2.27,
  x28:  2.28,
  x29:  -2.29,
  x30:  3.30,
  x31:  -3.31,
  x32:  3.32,
  x33:  -3.33,
  x34:  3.34,
  x35:  -3.35,
  x36:  3.36,
  x37:  -3.37,
  x38:  3.38,
  x39:  -3.39
}

export const tooltipFormatter = function () {
  return this.points.reduce(function (string, point) {
    return string + '<br/>' + point.series.name + ': ' +
      point.y + ` ${point.series.options.unit}`;
  }, '<b>' + new Date(this.x).toISOString() + '</b>');
}



export const getRandomizedStartEnd = () => {
  const randomIndex = (len) => ~~(Math.random() * len);

  const yString = [
    // "2008",
    // "2009",
    // "2010",
    // "2011",
    // "2012",
    // "2013",
    // "2014",
    // "2015",
    // "2016",
    // "2017",
    // "2018",
    // "2019",
    "2020",
    // "2021"
  ]
  const mString = [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12"
  ]
  const dString = [
    "01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
    "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
    "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
  ]

  const _start = new Date(`${yString[randomIndex(yString.length)]}-${mString[randomIndex(mString.length)]}-${dString[randomIndex(dString.length)]}`).getTime();
  const _end = new Date(`${yString[randomIndex(yString.length)]}-${mString[randomIndex(mString.length)]}-${dString[randomIndex(dString.length)]}`).getTime();

  const [start, end] = [_start, _end].sort();

  return {
    start,
    end
  }
}

export const xAxisFormatter = (value, range) => {

  const formatter = fstring => format(value, fstring,{ locale: ru })

  switch (true) {
    case range <= MINUTE: {
      return formatter('HH:mm:ss.SSS')
    }
    case range <= HOUR: {
      return formatter('HH:mm:ss')
    }
    case range <= DAY: {
      return formatter('HH:mm')
    }
    case range <= MONTH: {
      return formatter('yyy MMM dd')
    }
    default:
      return formatter('yyy MMM dd')
  }
}






















