const proxies = require('../server/proxies.js');

module.exports = function(app) {
  proxies(app);
};
