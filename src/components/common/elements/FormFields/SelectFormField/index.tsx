import React from 'react';
import {Field} from "redux-form";

export const SelectFormField = () => (
  <Field
    name="labelName"
    component="select"
  >
    <option>выберите одно из</option>
    <option value="#ff0000">Red</option>
    <option value="#00ff00">Green</option>
    <option value="#0000ff">Blue</option>
  </Field>
)
