import React from 'react';
import {Field} from "redux-form";

export const TextFormField = () => (
  <Field
    name="firstName"
    component="input"
    type="text"
    placeholder="First Name"
  />
)
