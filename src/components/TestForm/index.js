import React from 'react';
import {TextFormField} from "components/common/elements/FormFields";
import {SelectFormField} from "components/common/elements/FormFields";
import {reduxForm} from 'redux-form';
import {FORMS} from "../../constants";

const Form = () => <>
  <TextFormField/>
  <SelectFormField/>
</>

export const TestForm = reduxForm({form: FORMS.CONFIRM})(Form);

