import React, {useCallback, useState, useEffect} from 'react';
import {Chart, ANOMALIES_DEVIATIONS_VARIANTS} from 'smart-digit-design-system';
import {TimeseriesController, WebSocketController} from '../../api'
import {TIMESERIES_WS_PATH} from 'api/paths';
import {useToggler} from 'hooks';

const socket = new WebSocketController(TIMESERIES_WS_PATH);

const units = ["кг/м3", "%", "килограммы на метр"];
const dataFactory = (tagName, series) => ({
  "name": tagName,
  "data": [
    {
      "values": series
    }
  ],
  "legendTooltip": {
    "UUID": "R4.RVS-404.A0800.PD",
    "description": "Плотномер"
  },
  "measurementLimits": {},
  "limits": {
    "low": {},
    "high": {}
  },
  "uuid": tagName,
  "unit": units[~~(Math.random() * units.length)],
  "anomalies": [],
  "deviation": {
    "MOL": [],
    "SOL": [],
    "SDL": []
  }
})

const initialSeries = [
  {
    "name": "R4.RVS-404.A0800.PD",
    "data": [
      {
        "values": [1, 3, 4, 5]
      }
    ],
    "uuid": "R4.RVS-404.A0800.PD",
    "unit": "кг/м3",
    "legendTooltip": {
      "UUID": "R4.RVS-404.A0800.PD",
      "description": "Плотномер"
    },
    "measurementLimits": {},
    "limits": {
      "low": {},
      "high": {}
    },
    "anomalies": [],
    "deviation": {
      "MOL": [],
      "SOL": [],
      "SDL": []
    }
  }
]

const initialRange = {
  range: {
    startDate: 1598907600000,
    endDate: 1600117200000
  }
}


export const Gr = () => {
  const [mappedData, setMappedData] = useState(initialSeries);
  const [, toggleConnected] = useToggler(false);

  const [assetTree] = useState([
    {
      name: "УНХ",
      id: 1110000000012924,
      title: "УНХ",
      assetId: "RN.DN.TNPZ",
    }
  ]);
  const [allTags] = useState([
    {
      id: 'R4.RVS-404.A0800.FP',
      UUID: 'R4.RVS-404.A0800.FP',
      name: 'R4.RVS-404.A0800.FP',
      activeIds: [1110000000012924]
    },
    {
      id: 'R4.RVS-404.A0800.PP',
      UUID: 'R4.RVS-404.A0800.PP',
      name: 'R4.RVS-404.A0800.PP',
      activeIds: [1110000000012924]
    }
  ]);

  useEffect(() => {
    socket.init([onMessage, toggleConnected, onError]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [initialSettings] = useState(initialRange);
  const watchSettings = async ({requestParams}) => {
    const {tags: initialTags} = requestParams;
    const {tags} = await TimeseriesController.getHistorical(requestParams);
    const newSeries = initialTags.map(({tagName}) => {
      return dataFactory(tagName, tags[tagName])
    })
    setMappedData(newSeries);
  }

  const onRealTime = useCallback(async () => {
    const key = await TimeseriesController.getWSAccessKey();
    socket.connectAndSubscribe(key, 'R4.RVS-404.A0800.PD');
  }, []);

  const reload = useCallback(() => {
    updateSeries({
      tag: '1111',
      timestamp: new Date().getTime(),
      value: ~~(Math.random() * 1000000)
    }, true)
  }, []);

  const reload2 = useCallback(() => {
    updateSeries({
      tag: '1111',
      timestamp: new Date().getTime(),
      value: ~~(Math.random() * 1000000)
    }, false)
  }, []);

  const updateSeries = ({timestamp, value} = null, reset = false) => {
    const newPoint = [Number(timestamp), ~~(Math.random() * 1000000)];
    setMappedData((mappedData) => {
      return mappedData.map(seria => {
        return {
          ...seria,
          data: [
            {values: reset ? [] : [...seria.data[0].values, newPoint]}
          ]
        }
      });
    })
  };

  function onMessage({body}) {
    const data = JSON.parse(body);
    const {tag, ts: timestamp, val: value} = data;
    updateSeries({
      tag,
      timestamp,
      value
    })
  }

  function onError(frame) {
    console.log(frame);
  }

  useEffect(() => {
    console.log('>>>', mappedData);
  }, [mappedData])

  return <div>
    <button onClick={reload}>reload</button>
    <button onClick={reload2}>reload2</button>
    <Chart
      anomaliesDeviationsView={ANOMALIES_DEVIATIONS_VARIANTS.SHOW_ALL_ANOMALIES}
      onClickRealTime={onRealTime}
      series={mappedData}
      tags={allTags}
      watchSettings={watchSettings}
      checkedActives={[]}
      actives={assetTree}
      initialSettings={initialSettings}
    />
  </div>
};
