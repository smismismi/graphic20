import React, {useCallback} from 'react';
import {useDispatch} from "react-redux";
import { getCaseItemAction} from "store/modules/selectedItem";
import styles from './index.module.css';

type Params = {
  list: Array<number>,
  error: any,
  loading: any
}

export const InboxList = ({ list, error, loading } : Params) => {
  const dispatch = useDispatch();
  // const [, setTimer] = useState();

/*  useEffect(() => {
    let a = setInterval(() => {
      dispatch(getCasesAction())
    }, 2000);
    setTimer(() => {
      return a
    });
    return () => {
      clearInterval(a)
    }
  }, [dispatch])*/

  const selectItem = useCallback((id) => {
    dispatch({ type: 'ITEM_SUCCESS', data: id })
    dispatch(getCaseItemAction(id))
  }, [dispatch])

  if (loading) {
    return <div>крутилка</div>
  }
  if (error) {
    return <div>error</div>
  }
  return <div>
    InboxList
    { list?.map((element: number, index) => <li key={index} className={styles.listItem} onClick={() => selectItem(element)}>{element}</li>)}
  </div>
};
