import React, {useCallback, memo, useEffect, useState} from 'react';
import {InboxList} from './InboxList';
import {InboxCard} from './InboxCard';
import {useDispatch, useSelector} from "react-redux";
import { getCasesAction } from 'store/modules/cases'
type Params = {
  a: number,
  b: number
}
type fetchedData = {
  data: any,
  error: any,
  loading: any,
}

export const Inbox = memo(({a, b}: Params) => {
  const dispatch = useDispatch();
  const [show, setShow] = useState(true);
  const list = useSelector(
    (state: { cases: fetchedData }) => {
      return state.cases
    }
  );
  const selectedItem = useSelector(
    (state: { item: fetchedData }) => {
      return state.item
    }
  );
  useEffect(() => {
    dispatch(getCasesAction())
    return () => {}
  }, [dispatch])

  useEffect(() => {
    console.log(list);
  }, [list]);

  const { data, error, loading } = list;
  const { data: selectedItemData, error: selectedItemError, loading: selectedItemLoading } = selectedItem;

  const hide = useCallback(() => {
    setShow(prevShow => !prevShow)
  }, [setShow]);

  return <div>
    {a} = {b}
    <h1 onClick={hide}>fwegrt</h1>
    <table style={{width: '100%'}}>
      <thead>
      </thead>
      <tbody>
        <tr>
          <td>
            { show && <InboxList list={data} error={error} loading={loading}/> }
          </td>
          <td>
            <InboxCard item={selectedItemData} error={selectedItemError} loading={selectedItemLoading}/>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
});
