import React, {useEffect, useState, useCallback} from 'react';
import { Graphic } from 'components/Graphic';

type Params = {
  item: number,
  error: any,
  loading: any
}

const initialSeries = [
  {
    "name": "R4.RVS-404.A0800.PD",
    "data": [
      {
        "aggregation": "AVG",
        "values": [
          [
            1593718906000,
            0.44617096271939716
          ],
          [
            1593718911000,
            0.5009808603667893
          ],
          [
            1593718916000,
            1.2065154678955285
          ]
        ]
      }
    ],
    "tagInfo": {
      "fullName": "Плотномер",
      "timeZone": 3,
      "shortName": "R4.RVS-404.A0800.PD",
      "sourceTag": "",
      "synthetic": false,
      "assetClass": "TAG",
      "sourceSystem": "2",
      "parameterUnit": "KMQ",
      "processVariableType": "DEN",
      "originalParameterUnit": "KMQ",
      "displayUnit": "кг/м3"
    },
    "tagsLimits": {},
    "uuid": "R4.RVS-404.A0800.PD",
    "unit": "кг/м3",
    "legendTooltip": {
      "UUID": "R4.RVS-404.A0800.PD",
      "description": "Плотномер"
    },
    // @ts-ignore
    "measurementLimits": {},
    // @ts-ignore
    "limits": {
      "low": {},
      "high": {}
    },
    "anomalies": [],
    "deviation": {
      // @ts-ignore
      "MOL": [
        [
          1593718906000,
          1593718916000
        ]
      ],
      // @ts-ignore
      "SOL": [
        [
          1593718912000,
          1593718914000
        ]
      ],
      "SDL": []
    }
  }
]

export const InboxCard = ({ item, error, loading } : Params) => {
  const [series, setSeries] = useState(initialSeries)
  useEffect(() => {

  },[])

  const changeSeries = useCallback(() => {
    const newInitialSeries = initialSeries.map(item => {
      return {
        ...item,
        data: [
          {
            aggregation: 'AVG',
            values: [
              [
                1593718906000,
                0.2806997644605955 * Math.random() * 5
              ],
              [
                1593718911000,
                0.279873604617711 * Math.random() * 5
              ],
              [
                1593718916000,
                0.259873604617711 * Math.random() * 5
              ]
            ]
          }
        ]
      }
    })
    setSeries(newInitialSeries)
  },[])

  if (loading) {
    return <div>крутилка</div>
  }
  if (error) {
    return <div>error</div>
  }

  return <div>
    InboxCard
    <br/>
    <button onClick={changeSeries}>change</button>
    <Graphic series={series}/>
    { item }
  </div>
};
