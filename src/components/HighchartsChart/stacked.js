import React from "react";
import Highcharts from "highcharts";
import {
  HighchartsChart,
  Highcharts3dChart,
  withHighcharts,
  XAxis,
  YAxis,
  ZAxis,
  Title,
  Subtitle,
  Legend,
  LineSeries,
  Caption, ColumnSeries,
  Annotation
  // Trendline
} from "react-jsx-highcharts";

import addHighcharts3DModule from 'highcharts/highcharts-3d';
import addAnnotations from 'highcharts/modules/annotations';

addHighcharts3DModule(Highcharts);
addAnnotations(Highcharts);

const plotOptions = {
  column: {
    stacking: 'normal',
    pointPadding: 0,
    edgeWidth: 13,
    edgeColor: 'rgba(0,0,0,0.01)'
  },
  series: {
    borderWidth: 5,
    borderColor: 'rgba(0,0,0,0.1)',
    borderRadius: 5,
    className: "qfewrwtjr"
  },
  animation: {
    duration: 9000
  }
};
const containerProps = {
  style: {
    width: '100vw',
    height: '80vh'
  },
  'data-cy': 'My Cypress.io selector',
  margin: 0
}
const depth = 100
const App = () => (
  <div className="app">
    <Highcharts3dChart
      animation={false}
      margin={40}
      frame={{
        bottom: {
          size: 1,
          color: 'rgba(0,0,0,1)'
        }
      }}
      containerProps={containerProps}
      plotOptions={plotOptions}
      alpha={24}
      beta={24}
      depth={depth}
      viewDistance={0}
    >
      <XAxis categories={['Apples', 'Oranges']} lineWidth={3} id={"myXAxis"}>
      </XAxis>
      <YAxis min={0} id={"myYAxis"}>
        <ColumnSeries
          crosshair={true}
          name="Column"
          type="Stacked"
          data={[8, 7]}
          pointPlacement="between"
          depth={depth}
        />
        <ColumnSeries
          name="Column"
          type="Stacked"
          data={[18, 27]}
          pointPlacement="between"
          depth={depth}
          borderRadius={11}
        />
        <ColumnSeries
          name="Column"
          type="Stacked"
          data={[{y: 44, color: {
              linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
              stops: [
                [0, '#FFF'],
                [1, '#333']
              ]
            }}, {y: 11, color: "#eafe12"}]}
          pointPlacement="between"
          depth={depth}
        />
        <ColumnSeries
          name="Column"
          type="Stacked"
          data={[34, 22]}
          pointPlacement="between"
          depth={depth}
        />
      </YAxis>
      <Annotation
        shapes={[
          {
            type: "path", stroke: "red", fill: "red",
            points: [
              {x: 4, y: 0},
              {x: 50, y: 60}
            ],
            markerEnd: 'arrow'
          }
        ]}
        draggable={false}
        options3d={{
          enabled: true,
          alpha: 45,
          beta: 0
        }}/>
    </Highcharts3dChart>
  </div>
);

export const Stacked = withHighcharts(App, Highcharts);
