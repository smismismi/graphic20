import React from "react";
import Highcharts from "highcharts";
import {
  HighchartsChart,
  withHighcharts,
  PieSeries,
  Series,
  XAxis,
  YAxis,
  ColumnSeries
} from "react-jsx-highcharts";

const pieData = [
  {
    name: "Jane",
    y: 13
  },
  {
    name: "John",
    y: 213
  },
  {
    name: "Joe",
    y: 19
  }
];

const App = () => (
  <div className="app">
    <HighchartsChart>
      <PieSeries name="Installation" data={pieData} />
    </HighchartsChart>
    <HighchartsChart>
      <XAxis></XAxis>
      <YAxis min={-15}>
        <ColumnSeries
          name="Column"
          data={[8, 7, 6, 5, 4, 3, -12, 1]}
          pointPlacement="between"
        />
      </YAxis>
    </HighchartsChart>
    <HighchartsChart data={[1, 2, 3, 4, 5, 6]}>
      <XAxis>
        <XAxis.Title>X Axis</XAxis.Title>
      </XAxis>
      <YAxis id="myYaxis">
        <Series
          name={"wefwegweg"}
          type={"line"}
          opacity={0.5}
          data={[
            12908,
            { y: 216.4, color: "#BF0B23", size: 12 },
            8105,
            11248,
            8989,
            11816,
            18274,
            18111
          ]}
        />
        <Series
          name={"wefwegweg"}
          type={"line"}
          opacity={0.9}
          marker={{ enabled: true }}
          data={[
            { y: 216.4, color: "#BF0B23", size: 12 },
            8989,
            11816,
            12908,
            { y: 216.4, color: "#BF0B23", size: 12, marker: { enabled: false } },
            8105,
            18274,
            18111
          ]}
          zones={[
            {
              value: 8000,
              color: '#f7a35c'
            }, {
              value: 15000,
              color: '#7cb5ec'
            }, {
              color: '#90ed7d'
            }
          ]}
        />
      </YAxis>
    </HighchartsChart>
  </div>
);

export const ThreeD = withHighcharts(App, Highcharts);
