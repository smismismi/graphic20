import React from "react";
import Highcharts from "highcharts";
import {
  HighchartsChart,
  withHighcharts,
  XAxis,
  YAxis,
  ZAxis,
  Title,
  Subtitle,
  Legend,
  LineSeries,
  Caption,
  SolidGaugeSeries,
  Pane
  // Trendline
} from "react-jsx-highcharts";

import addSolidGaugeModule from 'highcharts/modules/solid-gauge';
addSolidGaugeModule(Highcharts);

const plotOptions = {
  solidgauge: {
    dataLabels: {
      y: 88,
      font: 55,
      borderWidth: 1,
      useHTML: true
    }
  }
};

const App = () => (
  <div className="app">
    <HighchartsChart
      gauge
      plotOptions={plotOptions}
    >
      <Pane
        // center={['50%', '85%']}
        size='100%'
        startAngle={-120}
        endAngle={120}
        background={{
          backgroundColor: '#EEE',
          innerRadius: '10%',
          outerRadius: '70%',
          shape: 'arc'
        }} />
      <XAxis />
      <YAxis
        lineWidth={1}
        minorTickInterval={null}
        tickPixelInterval={100}
        tickWidth={1}
        labels={{
          y: 0
        }}
        min={0}
        max={100}>
        <SolidGaugeSeries
          name='Speed'
          data={[ 95 ]}
        />
      </YAxis>

    </HighchartsChart>
  </div>
);

export const Gauge = withHighcharts(App, Highcharts);
