import React, { useState, useCallback } from "react";
import Highcharts from "highcharts";
import {
  HighchartsChart,
  Highcharts3dChart,
  withHighcharts,
  PieSeries,
  Series,
  ScatterSeries,
  XAxis,
  YAxis,
  ZAxis,
  Annotation,
  ColumnSeries
} from "react-jsx-highcharts";

import addHighchartsMore from 'highcharts/highcharts-more';
import addHighcharts3DModule from 'highcharts/highcharts-3d';
import addAnnotations from 'highcharts/modules/annotations';

addHighchartsMore(Highcharts);
addHighcharts3DModule(Highcharts);
addAnnotations(Highcharts);

const pieData = [
  {
    name: "Jane",
    y: 13
  },
  {
    name: "John",
    y: 213
  },
  {
    name: "Joe",
    y: 19
  }
];

const plotOptions = {
    series: {
      borderRadius: 15
    }
}

const chartOptions = {
  backgroundColor: '#f00',
  options3d: {
    alpha: 45,
    beta: 22
  }
}
const pieOptions = {
  pie: {
    innerSize: 100,
    depth: 145
  }
}
const radian = Math.PI / 360;
const numRows = 2;
const data = [];
const d = 360 / numRows;
const step = d * radian;

for (let i = 0; i < numRows; i++) {
  const iCos = Math.cos(i * step);
  for (let j = 0; j < numRows; j++) {
    const value = iCos * Math.cos(j * step);
    data.push([ i, value, j ]);
  }
}

console.log(data);

const App = () => {
  const [deg, setDeg] = useState(0);
  const [deg2, setDeg2] = useState(0);

  const changeHandler = useCallback((e) => {
    if (e.keyCode === 38) {
      setDeg(prevValue => --prevValue)
    }
    if (e.keyCode === 40) {
      setDeg(prevValue => ++prevValue)
    }
  });
  const changeHandler2 = useCallback((e) => {
    if (e.keyCode === 38) {
      setDeg2(prevValue => --prevValue)
    }
    if (e.keyCode === 40) {
      setDeg2(prevValue => ++prevValue)
    }
  });

  return <div className="app">
    <Highcharts3dChart alpha={65} plotOptions={pieOptions}>
      <PieSeries name="Installation" data={pieData} pieOptions={pieOptions} options3d={{
        enabled: true,
        alpha: 45,
        beta: 0
      }}/>
    </Highcharts3dChart>
    <Highcharts3dChart alpha={130} beta={20} depth={300} plotOptions={plotOptions} fitToPlot={true}>
      <XAxis></XAxis>
      <YAxis min={-15}>
        <ColumnSeries
          name="Column"
          data={[8, 7, 6, 5, 4, 3, -12, 1]}
          pointPlacement="between"
        />
      </YAxis>
    </Highcharts3dChart>
    <input type="text" value={deg} onKeyDown={changeHandler}/>
    <input type="text" value={deg2} onKeyDown={changeHandler2}/>
    <Highcharts3dChart  alpha={deg2}  beta={deg} depth={300} >
      <XAxis id="myXaxis"/>
      <YAxis id="myYaxis" min={-1.2} max={1.2} />
      <ZAxis id="myZaxis" min={-1.2} max={1.2} >
        <ScatterSeries data={data} labels={{ enabled: false }}/>
      </ZAxis>
      {/*<Annotation*/}
      {/*  shapes={[*/}
      {/*    {*/}
      {/*      type: "path", stroke: "red", fill: "red",*/}
      {/*      points: [*/}
      {/*        {xAxis: "myXaxis", yAxis: "myYaxis", x: 4, y: 0},*/}
      {/*        {xAxis: "myXaxis", yAxis: "myYaxis", x: 5, y: 6}*/}
      {/*      ],*/}
      {/*      markerEnd: 'arrow'*/}
      {/*    }*/}
      {/*  ]}*/}
      {/*  draggable={false}*/}
      {/*  options3d={{*/}
      {/*    enabled: true,*/}
      {/*    alpha: 45,*/}
      {/*    beta: 0*/}
      {/*  }}/>*/}
    </Highcharts3dChart>
    <HighchartsChart plotOptions={plotOptions}>
      <XAxis id="myXaxis"/>
      <YAxis id="myYaxis" min={-15} max={100}>
        <ColumnSeries
          name="Column"
          data={[8, 7, 6, 5, 4, 3, -12, 1]}
          pointPlacement="between"
        />
        <ColumnSeries
          name="Column"
          data={[18, 7, 26, 5, 41, 3, -12, 1]}
          pointPlacement="between"

        />
      </YAxis>
    </HighchartsChart>
    {/*<HighchartsChart data={[1, 2, 3, 4, 5, 6]}>*/}
    {/*  <XAxis>*/}
    {/*    <XAxis.Title>X Axis</XAxis.Title>*/}
    {/*  </XAxis>*/}
    {/*  <YAxis id="myYaxis">*/}
    {/*    <Series*/}
    {/*      name={"wefwegweg"}*/}
    {/*      type={"line"}*/}
    {/*      data={[*/}
    {/*        12908,*/}
    {/*        { y: 216.4, color: "#BF0B23", size: 12 },*/}
    {/*        8105,*/}
    {/*        11248,*/}
    {/*        8989,*/}
    {/*        11816,*/}
    {/*        18274,*/}
    {/*        18111*/}
    {/*      ]}*/}
    {/*    />*/}
    {/*  </YAxis>*/}
    {/*</HighchartsChart>*/}
  </div>
};

export const ThreeD3 = withHighcharts(App, Highcharts);
