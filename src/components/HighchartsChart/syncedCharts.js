import React, { useState } from "react";
import Highcharts from "highcharts";
import {
  HighchartsChart,
  withHighcharts,
  XAxis,
  YAxis,
  Title,
  Series,
  Tooltip
} from "react-jsx-highcharts";

Highcharts.Pointer.prototype.reset = () => {};

Highcharts.Point.prototype.highlight = function (event) {
  this.onMouseOver(); // Show the hover marker
  this.series.chart.tooltip.refresh(this); // Show the tooltip
  this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
};

const App = () => {
  const [chartData] = useState({
    xData: [0.001567, 0.011765, 0.022194, 0.032316, 0.04266],
    datasets: [
      {
        name: "Speed",
        data: [13.833, 12.524, 11.441, 10.651, 9.961],
        unit: "km/h",
        type: "line",
        valueDecimals: 1
      },
      {
        name: "Elevation",
        data: [111116.857, 211117, 2211117.111, 11117.2, 222227.272],
        unit: "bpm",
        type: "area",
        valueDecimals: 0
      }
    ]
  });
  // const handleMouseMove = useCallback((e) => {
  //   let point = null;
  //   let event = null;
  //   Highcharts.charts.forEach((chart) => {
  //     console.log(chart);

  //     event = chart.pointer.normalize(e); // Find coordinates within the chart
  //     point = chart.series[0].searchPoint(event, true); // Get the hovered point
  //     if (point) {
  //       point.highlight(e);
  //     }
  //   });
  // }, []);

  const renderChart = (dataset, index) => {
    const tooltipPositioner = function () {
      return { x: this.chart.chartWidth - this.label.width, y: 10 };
    };
    const data = dataset.data.map((val, i) => [chartData.xData[i], val]);
    const colour = Highcharts.getOptions().colors[index];

    return (
      <HighchartsChart key={index}>
        <Title align="left">{dataset.name}</Title>
        <XAxis id="myXaxis" crosshair labels={{ format: "{value} km" }} />
        <YAxis id="myYaxis">
          <Series
            name={dataset.name}
            type={dataset.type}
            data={data}
            color={colour}
            tooltip={{ valueSuffix: ` ${dataset.unit}` }}
          />
        </YAxis>
        <Tooltip
          positioner={tooltipPositioner}
          borderWidth={0}
          backgroundColor="none"
          pointFormat="{point.y}"
          headerFormat=""
          shadow={false}
          style={{ fontSize: "18px" }}
          valueDecimals={dataset.valueDecimals}
        />
      </HighchartsChart>
    );
  };
  return (
    <div className="app">
      <div>{chartData.datasets.map(renderChart)}</div>
    </div>
  );
};
/*
class App2 extends Component {
  constructor(props) {
    super(props);

    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.renderChart = this.renderChart.bind(this);
    this.state = {
      chartData: null
    };
  }

  componentDidMount() {
    this.setState({
      chartData: {
        xData: [0.001567, 0.011765, 0.022194, 0.032316, 0.04266],
        datasets: [
          {
            name: "Speed",
            data: [13.833, 12.524, 11.441, 10.651, 9.961],
            unit: "km/h",
            type: "line",
            valueDecimals: 1
          },
          {
            name: "Elevation",
            data: [111116.857, 211117, 2211117.111, 11117.2, 222227.272],
            unit: "bpm",
            type: "area",
            valueDecimals: 0
          }
        ]
      }
    });
  }

  renderChart(dataset, index) {
    const tooltipPositioner = function () {
      return { x: this.chart.chartWidth - this.label.width, y: 10 };
    };
    const data = dataset.data.map((val, i) => [
      this.state.chartData.xData[i],
      val
    ]);
    const colour = Highcharts.getOptions().colors[index];

    return (
      <HighchartsChart key={index}>
        <Title align="left" margin={30} x={30}>
          {dataset.name}
        </Title>
        <XAxis crosshair labels={{ format: "{value} km" }} />
        <YAxis>
          <Series
            name={dataset.name}
            type={dataset.type}
            data={data}
            color={colour}
            tooltip={{ valueSuffix: ` ${dataset.unit}` }}
          />
        </YAxis>

        <Tooltip
          positioner={tooltipPositioner}
          borderWidth={0}
          backgroundColor="none"
          pointFormat="{point.y}"
          headerFormat=""
          shadow={false}
          style={{ fontSize: "18px" }}
          valueDecimals={dataset.valueDecimals}
        />
      </HighchartsChart>
    );
  }

  handleMouseMove(e) {
    // let point = null;
    // let event = null;

    Highcharts.charts.forEach((chart) => {
      event = chart.pointer.normalize(e); // Find coordinates within the chart
      // point = chart.series[0].searchPoint(event, true); // Get the hovered point
      // if (point) {
      //   point.highlight(e);
      // }
    });
  }

  render() {
    const { chartData } = this.state;
    if (!chartData) return null;

    return (
      <div className="app">
        <div onMouseMove={this.handleMouseMove}>
          {chartData.datasets.map(this.renderChart)}
        </div>
      </div>
    );
  }
}
*/
export const SyncedCharts = withHighcharts(App, Highcharts);
