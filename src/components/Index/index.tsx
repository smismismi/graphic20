import React, {useCallback, memo, useMemo, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { openModal } from 'store/modules/modals';
import { MODAL_IDS } from '../../constants';
import {TestForm} from "components/TestForm";


type Params = {
  a: number,
  b: string
}

const Index = memo(({a, b}: Params) => {
  const dispatch = useDispatch();
  const state = useSelector(
    (state) => state
  );

  const richCalc = useMemo(() => {
    return a + b
  }, [a, b])

  useEffect(() => {
    console.log(a, b, state);
    return
  }, [a, b, state])

  const openFormHandler = useCallback(() => {
    dispatch(openModal(MODAL_IDS.CONFIRM, {}));
  }, [dispatch]);

  const openFormHandler2 = useCallback(() => {
    dispatch(openModal(MODAL_IDS.CREATE, {}));
  }, [dispatch]);

  const clickMe = useCallback(() => {
    console.log(a);
  }, [a])

  return <div>
    <TestForm/>
    <span onClick={clickMe}>1</span>
    +{richCalc}+
    <span>{a}</span>
    <span>{b}</span>
    <div onClick={openFormHandler}>openForm</div>
    <div onClick={openFormHandler2}>openForm2</div>
  </div>
});

export default Index
