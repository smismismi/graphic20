import React, {useCallback} from 'react';
import {connect} from 'react-redux';
import {getModalPayload} from 'store/helpers';
import {closeModal} from 'store/modules/modals';
import {MODAL_IDS} from '../../../constants';
import {useDispatch} from 'react-redux';
import {TestForm} from "components/TestForm";

interface Props {
  visible: boolean,
  onSubmit: (event: React.MouseEvent<HTMLButtonElement>) => void
}

export const ConfirmComponent = ({visible, onSubmit}: Props) => {
  const dispatch = useDispatch();
  const closeModalHandler = useCallback(() => {
    dispatch(closeModal(MODAL_IDS.CONFIRM));
  }, [dispatch]);

  return (
    <>
      {visible && <div>
        CREATE
        <TestForm/>
        <span onClick={onSubmit}>click me</span>
        <span onClick={closeModalHandler}>closeModal me</span>
      </div>}
    </>
  );
};


const mapDispatchToProps = {
  closeModal
};

interface State {
  modals: object
}

const mapStateToProps = ({modals}: State) => ({
  ...getModalPayload(modals, MODAL_IDS.CONFIRM)
});

export const Confirm = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmComponent);

