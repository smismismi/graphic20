import React from 'react';
import { connect } from 'react-redux';

import { Confirm } from 'components/Modals/Confirm';
import { Create } from 'components/Modals/Create';
import { isModalVisible } from 'store/helpers';
import { MODAL_IDS } from '../../constants';

interface Props {
  isConfirmVisible: boolean
  isCreateVisible: boolean
}


const Modals = ({ isConfirmVisible, isCreateVisible }: Props) => {
  return (
    <>
      <Confirm
        visible={isConfirmVisible}
        onSubmit={() => {
          console.log('isConfirmVisible');
        }}
      />
      <Create
        visible={isCreateVisible}
        onSubmit={() => {
          console.log('isCreateVisible');
        }}
      />
    </>
  );
};

interface State {
  modals: object
}

const mapStateToProps = ({ modals }: State) => ({
  isConfirmVisible: isModalVisible(modals, MODAL_IDS.CONFIRM),
  isCreateVisible: isModalVisible(modals, MODAL_IDS.CREATE),
});

export const ModalsContainer = connect(mapStateToProps)(Modals);
