import React, {useCallback} from 'react';
import {connect} from 'react-redux';
import {getModalPayload} from 'store/helpers';
import {closeModal} from 'store/modules/modals';
import {MODAL_IDS} from '../../../constants';
import {useDispatch} from 'react-redux';

interface Props {
  visible: boolean,
  onSubmit: (event: React.MouseEvent<HTMLButtonElement>) => void
}

export const CreateComponent = ({visible, onSubmit}: Props) => {
  const dispatch = useDispatch();
  const closeModalHandler = useCallback(() => {
    dispatch(closeModal(MODAL_IDS.CREATE));
  }, [dispatch]);

  return (
    <>
      {visible && <div>
        COMFIRM
        <span onClick={onSubmit}>click me</span>
        <span onClick={closeModalHandler}>closeModal me</span>
      </div>}
    </>
  );
};

const mapDispatchToProps = {
  closeModal
};

interface State {
  modals: object
}

const mapStateToProps = ({modals}: State) => ({
  ...getModalPayload(modals, MODAL_IDS.CONFIRM)
});

export const Create = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateComponent);

