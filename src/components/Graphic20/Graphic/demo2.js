import React, {useEffect, useState, useRef, useCallback} from 'react';
import {difference, debounce, uniqBy} from 'lodash';
import {tooltipFormatter, getRandomizedStartEnd, xAxisFormatter, HOUR, MINUTE, DAY, MONTH} from 'utils'
import {Chip, DatePickerRange, CHIP_SIZE} from "smart-digit-design-system";

import {
  Chart,
  HighchartsStockChart,
  Navigator,
  Scrollbar,
  Legend,
  Series,
  withHighcharts,
  XAxis,
  YAxis
} from "react-jsx-highstock";
import Highcharts from "highcharts/highstock";

// import noDataToDisplay from 'highcharts/modules/no-data-to-display';
import {_, initLoadNavigatorData, initLoadData, matchUnits, getUnit} from "./meta"

// noDataToDisplay(Highcharts);

Highcharts.setOptions({
  lang: {
    loading: 'Загрузка...',
    rangeSelectorFrom: "С",
    rangeSelectorTo: "По",
    months: [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь',
    ],
    weekdays: [
      'Понедельник',
      'Вторник',
      'Среда',
      'Четверг',
      'Пятница',
      'Суббота',
      'Воскресенье',
    ],
    shortWeekdays: [
      'Пн',
      'Вт',
      'Ср',
      'Чт',
      'Пт',
      'Сб',
      'Вс',
    ],
    shortMonths: [
      'Янв',
      'Фев',
      'Мар',
      'Апр',
      'Май',
      'Июн',
      'Июл',
      'Авг',
      'Сен',
      'Окт',
      'Нояб',
      'Дек',
    ]
  }
})

const xAxisLabel = (e) => {
  const {value} = e;
  if (!Number.isInteger(value)) return false;
  const range = e.chart.xAxis[0].max - e.chart.xAxis[0].min;
  return xAxisFormatter(value, range)
}

const xAxisNavigatorLabel = (e) => {
  const {value} = e;
  if (!Number.isInteger(value)) return false;
  const range = e.chart.navigator.xAxis.max - e.chart.navigator.xAxis.min;
  return xAxisFormatter(value, range)
}

const xAxisEvents = {
  afterSetExtremes: debounce((e) => {
    const {chart} = e.target;
    const tagNames = chart.yAxis.reduce((acc, axis) => {
      if (axis?.options?.id !== 'navigator-y-axis') {
        return [...acc, ...axis?.options?.tagNames]
      }
      return acc
    }, []);
    const {userMin: start, userMax: end} = e;
    const interval = {
      start: Math.round(start),
      end: Math.round(end)
    };
    chart.options.chartChangeHandler(interval);
    initLoadData(chart, tagNames.map(({tagName}) => tagName), false)
    initLoadNavigatorData(chart, tagNames.map(({tagName}) => tagName), false)
  }, 350)
}

const setExtremes = (chart, start, end, timeout = 0, plot = true, navigator = true) => {
  const ushki = end - start;
  const {start0, end0} = {
    start0: start - ushki,
    end0: (end + ushki) > new Date().getTime() ? new Date().getTime() : (end + ushki),
  }
  chart && plot && chart.xAxis[0].setExtremes(start, end);
  chart && navigator && chart.navigator.xAxis.setExtremes(start0, end0);
};

const App = (props) => {
  const {okno, tags} = props;
  const [chart, setChart] = useState(null);
  const [interval, setStateInterval] = useState({
    start: null,
    end: null
  });

  const _start = useRef();
  const _end = useRef();
  const _unblock = useRef(true);
  const _tags = useRef([]);

  useEffect(() => {
    const {start, end} = okno;
    if (chart && start && end && tags?.length) {
      if (_start.current !== start && _end.current !== end) {
        _unblock.current = true;
        setStateInterval({
          ...okno
        })
      }
    }
  }, [okno, chart]);
  //
  useEffect(() => {
    const {start, end} = interval;
    if (start && end) {
      if (_start.current !== start && _end.current !== end) {
        _start.current = start;
        _end.current = end;
        if (_unblock.current) {
          setExtremes(chart, start, end);
          _unblock.current = true;
        }
      }

      const added = difference(tags, _tags.current);
      const removed = difference(_tags.current, tags);

      if (added && added.length) {
        const mapped = added.map(({tagName}) => tagName);
        setTimeout(() => {
          initLoadData(chart, mapped, true);
          initLoadNavigatorData(chart, mapped, true);
        }, 250);
      }
      if (added.length || removed.length) {
        _tags.current = tags;
      }
    }
  }, [interval, tags, chart])

  const xAxisProps = {
    minRange: -5,
    labels: {
      formatter: xAxisLabel
    },
    tickPixelInterval: 250,
    // tickInterval: 30 * 24 * 60 * 60 * 1000,
    events: xAxisEvents,
  }

  const chartEvents = {
    load: function (e) {
      console.log('LOAD HIGHCHART');
      setChart(e.target)
    }
  }

  const chartProps = {
    chart: {
      height: 680,
      events: chartEvents,
      // animation: false
    },
    tooltip: {
      backgroundColor: '#FCFFC5',
      formatter: tooltipFormatter,
      shared: true
    }
  }

  const renderSeries = ({tagName, unit}) => {
    return <Series
      name={tagName}
      unit={unit}
      key={`rawDataId_${tagName}`}
      id={`rawDataId_${tagName}`}
      showInNavigator={true}
    />
  }

  const setRandomizedStartEnd = useCallback(debounce(() => {
    const {start, end} = getRandomizedStartEnd();
    const interval = {
      start,
      end
    }
    _unblock.current = true;
    setStateInterval(interval);
  }, 200), [chart])

  const setCustomIntervalFromNow = useCallback(debounce((interval) => {
    const NOW = new Date().getTime()
    const {start, end} = {
      start: NOW - interval,
      end: NOW,
    }
    const newInterval = {
      start,
      end
    }
    _unblock.current = true;
    setStateInterval(newInterval);
  }, 100), [chart])

  const applyCustomInterval = useCallback(() => {
    const start = new Date('2015-09-12').getTime();
    const end = new Date('2015-09-15').getTime();
    const interval = {
      start,
      end
    }
    _unblock.current = true;
    setStateInterval(interval);
  }, [interval, chart])

  const changeCalendarRange = useCallback((start, end) => {
    if (_start.current !== start && _end.current !== end) {
      const interval = {
        start,
        end
      }
      _unblock.current = true;
      setStateInterval(interval);
    }
  }, [])

  const chartChangeHandler = useCallback((interval) => {
    _unblock.current = false;
    setStateInterval(interval);
  }, [])


  const settleYAxis = tags => {
    const uniqAxis = uniqBy(tags, 'unit');
    return uniqAxis.map(({unit} = '') => {
      const thisAxiosTags = tags.filter(tag => tag.unit === unit);
        return <YAxis
          key={unit}
          tagNames={thisAxiosTags}
          unit={unit}
        >
          {
            thisAxiosTags?.length &&
            thisAxiosTags.map(renderSeries)
          }
        </YAxis>
      }
    )
  }

  return (
    <div className="app">
      <button onClick={setRandomizedStartEnd}>set randomized start end</button>
      <Chip size={CHIP_SIZE.SMALL} label={`c 2015-09-12 по 2015-09-15`} onClick={applyCustomInterval}/>
      <Chip size={CHIP_SIZE.SMALL} label={`20 сек`} onClick={() => setCustomIntervalFromNow(20 * 1000)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`1 мин`} onClick={() => setCustomIntervalFromNow(MINUTE)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`5 мин`} onClick={() => setCustomIntervalFromNow(5 * MINUTE)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`1 час`} onClick={() => setCustomIntervalFromNow(HOUR)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`1 день`} onClick={() => setCustomIntervalFromNow(DAY)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`1 неделя`} onClick={() => setCustomIntervalFromNow(7 * DAY)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`1 месяц`} onClick={() => setCustomIntervalFromNow(MONTH)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`3 месяц`} onClick={() => setCustomIntervalFromNow(3 * MONTH)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`9 месяц`} onClick={() => setCustomIntervalFromNow(9 * MONTH)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`9 месяц`} onClick={() => setCustomIntervalFromNow(9 * MONTH)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`10 месяц`} onClick={() => setCustomIntervalFromNow(10 * MONTH)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`11 месяц`} onClick={() => setCustomIntervalFromNow(11 * MONTH)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`12 месяц`} onClick={() => setCustomIntervalFromNow(12 * MONTH)}/>
      <Chip size={CHIP_SIZE.SMALL} label={`1 год`} onClick={() => setCustomIntervalFromNow(12 * MONTH)}/>

      <HighchartsStockChart
        {...chartProps}
        tags={tags}
        chartChangeHandler={chartChangeHandler}
      >
        <Chart zoomType="x"/>
        <XAxis {...xAxisProps} />
        {
          settleYAxis(tags)
        }
        <Navigator
          adaptToUpdatedData={false}
          series={{
            type: 'line'
          }}
          xAxis={{
            labels: {
              formatter: xAxisNavigatorLabel
            }
          }}
        />
        <Scrollbar/>
        <Legend layout="vertical" align="right" verticalAlign="top" width={250}/>
      </HighchartsStockChart>
      <DatePickerRange changeHandler={changeCalendarRange}
                       alwaysShowCalender={true}
                       showAcceptButton={true}
                       startDate={interval.start} endDate={interval.end} isOpen={false} />
    </div>
  );
}

export const Graphic = withHighcharts(App, Highcharts);
