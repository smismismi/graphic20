import React, {useEffect, useState, useRef, useCallback} from 'react';
import {difference, debounce} from 'lodash';

import {
  Chart,
  HighchartsStockChart,
  Navigator,
  RangeSelector,
  Scrollbar,
  Series,
  withHighcharts,
  XAxis,
  YAxis
} from "react-jsx-highstock";
import Highcharts from "highcharts/highstock";
import {_, checkIntervalToExtend, getNavigatorData, initLoadData, matchUnits, getUnit} from "./meta"


const xAxisEvents = {
  afterSetExtremes: debounce((e) => {
    const {userMin: start, userMax: end} = e;
    const {chart} = e.target;
    const tagNames = chart.yAxis[0]?.options?.tagNames;

    const interval = {
      start: start,
      end: end,
    }
    if (checkIntervalToExtend(chart, {start, end})) {
      if (tagNames && tagNames.length) {
        const ushki = end - start;
        const ushki0 = {
          start0: start - ushki,
          end0: end + ushki,
        }
        getNavigatorData(chart, ushki0.start0, ushki0.end0, tagNames)
        initLoadData(chart, interval.start, interval.end, tagNames)
      }
    } else {
      if (tagNames && tagNames.length) {
        initLoadData(chart, interval.start, interval.end, tagNames)
      }
    }
    chart.options.weigjwepg(interval);
  }, 500)
}

const App = (props) => {
  const {okno, tags} = props;
  const [chart, setChart] = useState(null);
  const [interval, setInterval] = useState({
    start: null,
    end: null
  });
  const [interval0, setInterval0] = useState({
    start0: null,
    end0: null,
  });

  const _start = useRef();
  const _end = useRef();
  const _tags = useRef([]);

  useEffect(() => {
    const {start, end} = okno;
    const ushki = end - start;
    const ushki0 = {
      start0: start - ushki,
      end0: end + ushki,
    }
    if (start && end && tags?.length) {
      if (_start.current !== start, _end.current !== end) {
        setInterval({
          ...okno,
          ...ushki0
        })
        setInterval0({
          ...ushki0
        })
      }
    }
  }, [okno]);

  useEffect(() => {
    const {start, end} = interval;
    const {start0, end0} = interval0;
    if (start && end && tags?.length) {
      const added = difference(tags, _tags.current);
      const removed = difference(_tags.current, tags);
    }
  }, [chart, interval, tags])

  const xAxisProps = {
    type: 'datetime',
    min: interval.start,
    max: interval.end,
    userMin: interval.start,
    userMax: interval.end,
    labels: {
      formatter: (e) => {
        const {value} = e;
        return new Date(+value).toLocaleDateString('ru-RU')
      }
    },
    events: xAxisEvents,
  }

  const chartEvents = {
    load: function (e) {
      console.log('LOAD HIGHCHART');
      setChart(e.target)
    }
  }

  const chartProps = {
    chart: {
      height: 300,
      events: chartEvents,
    },
    tooltip: {
      backgroundColor: '#FCFFC5',
      // formatter: function () {
      //   return this.points.reduce(function (s, point) {
      //     return s + '<br/>' + point.series.name + ': ' +
      //       point.y + 'm';
      //   }, '<b>' + new Date(this.x).toISOString() + '</b>');
      // },
      shared: true
    }
  }

  const renderSeries = (tag, index) => {
    return <Series
      tagName={tag}
      name={tag}
      interval={interval}
      key={`rawDataId_${tag}`}
      id={`rawDataId_${tag}`}
    />
  }

  const fillNavigation = (tag, index) => ({
    type: 'line',
    id: `navigatorDataId_${tag}`,
    tagName: tag,
    name: tag,
    data: []
  })

  const setIntervalwswf = useCallback(({start, end}) => {
    if (start && end) {
      if (_start.current !== start, _end.current !== end) {
        // setInterval(prevState => ({
        //     ...prevState,
        //     start,
        //     end
        //   })
        // )
      }
    }
  }, [])

  return (
    <div className="app">
      {_(interval.start)} = {_(interval.end)}
      <br/>
      {_(interval.start0)} = {_(interval.end0)}
      <br/>
      ============
      {_(_start.current)} = {_(_end.current)}
      {tags.length}
      <HighchartsStockChart
        {...chartProps}
        weigjwepg={setIntervalwswf}
        time={{
          timezoneOffset: 0 * 60
        }}
      >
        <Chart zoomType="x" />
        <XAxis {...xAxisProps} />
        <YAxis
          tagNames={tags}
          falseTrue={false}
        >
          {
            tags?.length &&
            tags.map(renderSeries)
          }
        </YAxis>
        <RangeSelector
          inputDateFormat={'%Y-%m-%d %H:%M:%S.%L'}
          inputEditDateFormat={'%Y-%m-%d %H:%M:%S.%L'}
          inputBoxWidth={250}
        >
          {/*<RangeSelector.Button count={1} offsetMax={10} offsetMin={1} type="millisecond">day</RangeSelector.Button>*/}
          {/*<RangeSelector.Button count={1} offsetMax={10} offsetMin={1} type="second">second</RangeSelector.Button>*/}
          {/*<RangeSelector.Button count={1} offsetMax={10} offsetMin={1} type="minute">minute</RangeSelector.Button>*/}
          {/*<RangeSelector.Button count={1} offsetMax={10} offsetMin={1} type="hour">hour</RangeSelector.Button>*/}
          {/*<RangeSelector.Button count={1} offsetMax={10} offsetMin={1} type="day">day</RangeSelector.Button>*/}
          {/*<RangeSelector.Button count={1} offsetMax={10} offsetMin={1} type="month">month</RangeSelector.Button>*/}
          {/*<RangeSelector.Button count={1} offsetMax={10} offsetMin={1} type="ytd">year</RangeSelector.Button>*/}
          <RangeSelector.Input boxBorderColor="#7cb5ec" enabled={true}/>
        </RangeSelector>

        <Navigator
          adaptToUpdatedData={false}
          series={
            tags?.length &&
            tags.map(fillNavigation)
          }
        />
        <Scrollbar/>
      </HighchartsStockChart>
    </div>
  );
}

export const Graphic = withHighcharts(App, Highcharts);
