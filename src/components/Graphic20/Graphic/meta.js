import {getHistoricalData} from "../../../store/modules/timeseries";
import {getAggregation, dataShuffler} from "../../../utils";

export const _ = date => `${new Date(+date || 0).toISOString()}, `;
export const THRESHOLD_PERCENT = .1;

export const checkIntervalToExtend = (chart) => {
  if (chart) {
    const {min, max} = chart.navigator.xAxis;
    const {min: min1, max: max1} = chart.xAxis[0];
    const percentEFeg = (max - min) * THRESHOLD_PERCENT;
    return (min + percentEFeg) > min1 || (max - percentEFeg) < max1
  }
  return false
}

export const loadData = async (chart, {start, end, aggregation, tagNames}) => {
  const {tags} = await getHistoricalData({
    start,
    end,
    tags: tagNames.map((tagName) => ({tagName})),
    aggregation
  })
  return tags
}

export const setData = (chart, data, force) => {
  Object.keys(data).forEach(tagData => {
    const seria = chart.series.find(item => item.userOptions.id === `rawDataId_${tagData}`)
    force && seria && seria.setData([]);
    seria && seria.setData(data[tagData]);
  })
}

export const setNavigatorData = (chart, data) => {
  Object.keys(data).forEach(tagData => {
    const seria = chart.navigator.series.find(item => item.baseSeries.options.name === tagData)
    seria && seria.setData(data[tagData]);
  })
}

export const initLoadData = (chart, tagNames, force) => {
  if (chart && tagNames?.length) {
    const start = chart.xAxis[0].min;
    const end = chart.xAxis[0].max;
    const promise = async () => {
      chart.showLoading();
      const aggregation = getAggregation(start, end);
      let data = await loadData(chart, {
        start,
        end,
        aggregation,
        tagNames
      })
      if (data) {
        data = dataShuffler(data)
        setData(chart, data, force);
      }
      chart.hideLoading();
    }
    promise()
  }
}
export const initLoadNavigatorData = (chart, tagNames, force) => {
  if (chart && tagNames?.length) {
    const start = chart.navigator.xAxis.min;
    const end = chart.navigator.xAxis.max;
    const promise = async () => {
      chart.showLoading();
      const aggregation = getAggregation(start, end);
      let data = await loadData(chart, {
        start,
        end,
        aggregation,
        tagNames
      })
      if (data) {
        data = dataShuffler(data)
        setNavigatorData(chart, data, force);
      }
      chart.hideLoading();
    }
    promise()
  }
}

export const getNavigatorData = (chart, start, end) => {
  const tagNames = chart.navigator.series.map(item => item.baseSeries.options.name);
  if (start && end) {
    const promise = async () => {
      const aggregation = getAggregation(start, end);
      let data = await loadData(chart, {
        start: start,
        end: end,
        aggregation,
        tagNames
      })

      if (data) {
        data = dataShuffler(data)
        setNavigatorData(chart, data, start, end);
      }
    }
    promise();
  }
}
