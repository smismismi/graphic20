import React, {useCallback, useEffect, useState} from 'react';
import { Graphic as GraphicS } from './demo2';
import {getTagsAction} from "../../../store/modules/tags";
import {getUnitsAction} from "../../../store/modules/tags";

// import {_} from "./meta";

const extendTags = async (tags, action, dataKey, key, setter) => {
  const data = await action(tags.map(({tagName}) => tagName));
  const extentedTags = tags.map(tag => ({
    ...tag,
    [key]: data[dataKey][tag.tagName]
  }))
  setter(extentedTags);
}

export const GraphicWrapped = (props) => {
  const {okno, tags} = props;

  const [allTags, setAllTags] = useState([])
  const [selectedTags, setSelectedTags] = useState([])
  const [extentedTags, setExtentedTags] = useState([])

  useEffect(() => {
    const promise = async () => {
      const {tags} = await getTagsAction()
      setAllTags(tags)
    }
    promise();
  }, [])

  useEffect(() => {
    setSelectedTags(tags.map(tagName => ({tagName})))
  }, [tags]);

  useEffect(() => {
    extendTags(selectedTags, getUnitsAction, 'units', 'unit', setExtentedTags)
  }, [selectedTags]);

  const handleSelectCheckbox = useCallback(({target}) => {
    const foundIndex = selectedTags.findIndex(tag => tag.tagName === target.value);
    if (foundIndex > -1) {
      const newSelectedTags = [...selectedTags]
      newSelectedTags.splice(foundIndex, 1)
      setSelectedTags(newSelectedTags)
    } else {
      setSelectedTags([...selectedTags, {tagName: target.value}])
    }
  }, [selectedTags]);


  return (
    <div className="app">
      <div>
        <h1>tag selector</h1>
        {allTags.map(tagName => {
          return <span key={tagName}>
            <label
              htmlFor={`tag_${tagName}`}>
              <input
                id={`tag_${tagName}`}
                checked={selectedTags.map(tag => tag.tagName).includes(tagName) ? 'checked' : ''}
                type="checkbox"
                onChange={handleSelectCheckbox}
                name={'selectedTags'}
                value={tagName}/>
              {tagName}
            </label>
          </span>
        })}
      </div>
      {okno && okno.start && okno.end && extentedTags && extentedTags.length && <GraphicS okno={okno} tags={extentedTags}/>}
    </div>
  );
}
