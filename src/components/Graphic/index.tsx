import React from 'react';
import {Chart, ANOMALIES_DEVIATIONS_VARIANTS} from 'smart-digit-design-system';

type Series = any;

export const Graphic = ({ series }: Series) => {
  return <div>
    <Chart
      anomaliesDeviationsView={ANOMALIES_DEVIATIONS_VARIANTS.SHOW_ANOMALIES_IN_TAG}
      series={series}
      tags={[]}
      checkedActives={[]}
      actives={[]}
      initialSettings={{
        range: {
          startDate: 1593718906000,
          endDate: 1593718916000
        }
      }}
    />
  </div>
};
