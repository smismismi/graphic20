export class RequestError extends Error {
  constructor(message) {
    super(message);
    this.name = 'Request Error';
    this.message = message;
  }
}

export class ValidationError extends Error {
  constructor(message) {
    super(message);
    this.name = 'Validation Error';
    this.message = message;
  }
}
