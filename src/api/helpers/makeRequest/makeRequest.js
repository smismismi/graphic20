import { SchemaValidator } from 'api/helpers/schemaValidator/schemaValidator';
import { RequestError } from 'api/helpers/errors';
import responseBodySchema from 'api/helpers/makeRequest/response.json';

const responseValidator = new SchemaValidator();

/**
 * @param path
 * @param method / options
 * @param body
 * @returns {Promise<Response>}
 */
export async function makeRequest(path, method, body) {
  const requestOptions = createOptions(method, body);
  try {
    const response = await fetch(path, requestOptions);
    return await handleResponse(response);
  } catch (error) {
    console.log(`${method} ${path}\n`, error);
    return Promise.reject(error);
  }
}

function createOptions(options, body) {
  if (options === undefined) {
    throw new RequestError('method or options not set');
  }

  const defaultOptions = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  switch (typeof options) {
    case 'string':
      const method = checkMethodName(options);
      if (method === 'post' || method === 'put') {
        return { ...defaultOptions, method, body: JSON.stringify(body) };
      }
      return { ...defaultOptions, method };
    case 'object':
      return { ...defaultOptions, ...options };
    default:
      break;
  }

  throw new RequestError('method or options are not set correctly');
}

function checkMethodName(method) {
  const requestMethod = method.toLowerCase();
  switch (requestMethod) {
    case 'post':
    case 'put':
    case 'get':
    case 'delete':
      return requestMethod;
    default:
      throw new RequestError('the HTTP Method is not set correctly');
  }
}

async function handleResponse(response) {
  const MAX_RES_BODY_LENGTH = 150;
  const contentType = response.headers.get('content-type');
  if (!contentType.includes('application/json')) {
    console.log(
      `Warning: Response (${response.url}) contains an unsupported header:\nContent-Type: ${contentType}`
    );
  }

  if (!response.ok) {
    const responseText = await response.text();
    throw new RequestError(`${response.status} ${response.statusText} \n
      Response body: ${responseText.slice(0, MAX_RES_BODY_LENGTH)} ...`);
  }

  const responseBody = await response.json();
  if (responseValidator.validate(responseBodySchema, responseBody)) {
    return responseBody.data;
  }

  throw new RequestError(
    `${response.status} ${response.statusText} \n
    Incorrect data structure in response body. Expected object: {data, message, success} \n
    Response body: ${responseBody.slice(0, MAX_RES_BODY_LENGTH)} ...`
  );
}
