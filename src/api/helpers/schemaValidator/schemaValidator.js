import Ajv from 'ajv';
import { ValidationError } from 'api/helpers/errors';

export class SchemaValidator {
  ajv;

  constructor(defsSchemas = []) {
    this.ajv = new Ajv({
      allErrors: true,
      verbose: true,
      useDefaults: true,
      schemas: defsSchemas
    });
  }

  validate(schema, data) {
    const isValidate = this.ajv.compile(schema);

    if (!isValidate(data)) {
      this.handlerError(isValidate.errors);
    }

    return data;
  }

  handlerError(errors) {
    const messages = errors.map(error => {
      switch (error.keyword) {
        case 'type':
          return `Wrong type of field ${error.dataPath}, ${error.message}.`;
        case 'required':
          return `Missing required field, ${error.message}.`;
        default:
          return `${error.keyword}, ${error.message}`;
      }
    });
    console.log(messages.join('\n'));
    throw new ValidationError(messages.join('\n'));
  }
}
