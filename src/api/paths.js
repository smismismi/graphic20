export const USERS = `https://jsonplaceholder.typicode.com/`;
export const CASES = `cases`;
export const TS = `timeseries-api`;
export const TAGS = `tags`;
export const UNITS = `units`;
export const TIMESERIES_WS_PATH =
  'timeseries-api-scpdr-uat-uat.run.ppc-jv-dev.sibintek.ru/timeseries-api/websocketEndpoint';

