import { TS } from './paths';
import { makeRequest } from './helpers';

export class TimeseriesController {
  static async getHistorical(data) {
    const orderedSeries = await makeRequest(
      `${TS}/historical/`,
      'POST',
      data
    );
    return { tags: orderedSeries };
  }
  static getWSAccessKey() {
    return makeRequest(`${TS}/livedata`, 'POST');
  }
}
