import { CASES } from './paths';
import { makeRequest } from './helpers';

export class CasesController {
  static async getCases() {
    return await makeRequest(
      `${CASES}`,
      'GET'
    );
  }
  static async getItem(id) {
    return await makeRequest(
      `${CASES}/${id}`,
      'GET'
    );
  }
}
