import { TAGS, UNITS } from './paths';
import { makeRequest } from './helpers';

export class TagsController {
  static async getTags() {
    const tags = await makeRequest(
      `${TAGS}`,
      'GET'
    );
    return { tags };
  }
  static async getUnits(tags) {
    const units = await makeRequest(
      `${UNITS}`,
      'POST',
      { tags }
    );
    return { units };
  }
}
