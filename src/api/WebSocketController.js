import { Client } from '@stomp/stompjs';

export class WebSocketController {
  constructor(path) {
    this.config.brokerURL = `wss://${path}`;
  }
  config = {
    reconnectDelay: 1000,
    heartbeatIncoming: 10000,
    heartbeatOutgoing: 10000,
    // debug: str => {
    //   console.log(`STOMP: ${str}`);
    // },
    onConnect: frame => {
      console.log('on Connect: ', frame);
      this.connected = true;
      this.onConnectStateChange(this.connected);
      this.subscribe();
    },
    onStompError: frame => {
      console.log(frame.body);

      this.connected = false;
      this.onConnectStateChange(this.connected);
      this.onError(frame.body);
    }
  };
  connected = false;
  stompClient = null;
  accessKey = null;
  tagName = null;

  init(actions) {
    const [onMessage, onConnectStateChange, onError] = actions;

    this.onError = onError;
    this.onConnectStateChange = onConnectStateChange;
    this.onMessage = onMessage;
  }

  connect() {
    // Create an instance
    this.config.connectHeaders = {
      'x-access-key': this.accessKey
    };
    console.log(this.config.connectHeaders);
    this.stompClient = new Client(this.config);
    console.log(this.stompClient);
    this.stompClient.logRawCommunication = true;
    // Attempt to connect
    this.stompClient.activate();
  }

  disconnect() {
    this.stompClient.deactivate();
    this.connected = false;
  }

  connectAndSubscribe(accessKey, tagName) {
    this.accessKey = accessKey;
    this.tagName = tagName;

    if (accessKey && tagName) {
      if (!this.connected) {
        this.connect();
      } else {
        this.subscribe();
      }
    }
  }

  subscribe() {
    this.subscription = this.stompClient.subscribe(
      `/exchange/tags.raw.data/${this.tagName}`,
      this.onMessage
    );
  }

  unsubscribe() {
    this.subscription.unsubscribe();
  }
}
