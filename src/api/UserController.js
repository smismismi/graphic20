import { USERS } from './paths';
import { makeRequest } from './helpers';

export class UserController {
  static async getUsersForTask(id, type, role) {
    const users = await makeRequest(
      `${USERS}/todos/`,
      'GET'
    );

    return users.map(({ data }) => ({ ...data }));
  }
}
