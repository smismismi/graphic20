const fs = require('fs');
const {data} = require('./mocks/_large-dataset4.json');

console.log(data.length);

// const set2 = {
//   ...set,
//   data: set.data.map((point, index) => ([set.pointStart + set.pointInterval * index, point]))
// }

// const set2 = {
//   data: data.filter((item, index) => index < 100000).reduce((acc, item) => {
//     return [...acc, [item[0] + (3632 * 24 * 60 * 60 * 1000), Math.round(item[1])], [item[0] + 30 * 1000 + (3632 * 24 * 60 * 60 * 1000), Math.round(item[1])]]
//   }, [])
// }
const efqwef = []

const set2 = {
  data: data.filter((item, index) => index < 600000).map(item => {
    efqwef.push([item[0] + (3632 * 24 * 60 * 60 * 1000), Math.round((item[1] + Math.random()) * 100) / 100]);
    efqwef.push([item[0] + 30 * 1000 + (3632 * 24 * 60 * 60 * 1000), Math.round((item[1] + Math.random()) * 100) / 100]);
  })
}

fs.writeFileSync('./mocks/output2.json', JSON.stringify({ data: efqwef }));

