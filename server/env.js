const { logger } = require('./utils/logger');

const {
  PORT,
  APP_SCOPE,
  UAA_AUTH_URL,
  CLIENT_ID,
  CLIENT_SECRET,
  USE_USER_TOKEN_CHECK,

  MEASURE_CASES_BACKEND,
  WORKFLOW_BACKEND,
  BACKEND_TECH,
  ACS_BACKEND,
  ASSETS_BACKEND,
  TIMESERIES_BACKEND,

  IS_DEV,
  USER_TOKEN,
  CLIENT_TOKEN
} = process.env;

module.exports = {
  PORT: getEnv(PORT, 'PORT', 'number', false, 9000),
  APP_SCOPE: getEnv(APP_SCOPE, 'APP_SCOPE', 'string', true),
  UAA_AUTH_URL: getEnv(UAA_AUTH_URL, 'UAA_AUTH_URL', 'string', true),
  CLIENT_ID: getEnv(CLIENT_ID, 'CLIENT_ID', 'string', true),
  CLIENT_SECRET: getEnv(CLIENT_SECRET, 'CLIENT_ID', 'string', true),
  USE_USER_TOKEN_CHECK: getEnv(
    USE_USER_TOKEN_CHECK,
    'USE_USER_TOKEN_CHECK',
    'bool',
    false,
    true
  ),

  MEASURE_CASES_BACKEND: getEnv(MEASURE_CASES_BACKEND, 'MEASURE_CASES_BACKEND', 'string', true),
  WORKFLOW_BACKEND: getEnv(
    WORKFLOW_BACKEND,
    'WORKFLOW_BACKEND',
    'string',
    true
  ),
  TIMESERIES_BACKEND: getEnv(
    TIMESERIES_BACKEND,
    'TIMESERIES_BACKEND',
    'string',
    true
  ),
  BACKEND_TECH: getEnv(BACKEND_TECH, 'BACKEND_TECH', 'string', true),
  ASSETS_BACKEND: getEnv(ASSETS_BACKEND, 'ASSETS_BACKEND', 'string', true),
  ACS_BACKEND: getEnv(ACS_BACKEND, 'ACS_BACKEND', 'string', true),
  USER_TOKEN: getEnv(USER_TOKEN, 'USER_TOKEN', 'string', false, ''),
  CLIENT_TOKEN: getEnv(CLIENT_TOKEN, 'CLIENT_TOKEN', 'string', false, ''),
  IS_DEV: getEnv(IS_DEV, 'IS_DEV', 'bool', true, false)
};

function getEnv(value, name, type, isRequired, defaultValue) {
  if (value === undefined && isRequired) {
    logger.error(`Error: Not found required environment - ${name} = ${value}`);
    process.exit(1);
  }

  if (value === undefined) {
    value = defaultValue;
    logger.info(
      `Warning: Not found environment - ${name}. Default value is used - ${name} = ${value}`
    );
  }

  switch (type) {
    case 'bool':
      return String(value) === 'true';
    case 'number':
      return Number(value);
    case 'string':
    default:
      return String(value);
  }
}
