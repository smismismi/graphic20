const express = require('express');
const proxy = require('express-http-proxy');
const {expressLogger} = require('./utils/logger');
const addClientToken = require('./middleware/addClientToken.middleware');
const {TIMESERIES_BACKEND} = require('./env');
// const {data} = require('./mocks/_03.json');
const {data} = require('./mocks/actual_data.json');
const {tags} = require('./mocks/tags.json');
const {units} = require('./mocks/units.json');

const {
  requestProxy,
  timeSeriesLivedataResDecorator,
} = require('./utils/requestProxy');

const mockGenerator = function (start, end) {

  const count = 25
  const interval = (end - start) / count;
  let values = [];

  for (let countValues = 0; countValues <= count; countValues++) {
    values.push([start + interval * countValues, ~~(Math.random() * 1000000)])
  }
  return values
}

const getData = function (start, end, aggregation = 0) {
  const efwe = data.filter((item) => (item[0] >= start) && (item[0] <= end));
  const efwefgwe = efwe.reduce((acc, item, index) => {
    acc.itemsCollector.push(item);
    if (index + 1 === efwe.length || acc.counter >= aggregation) {
      return {
        counter: 0,
        data: [...acc.data, [acc.itemsCollector.reduce((acc, item) => acc + item[0], 0) / acc.itemsCollector.length, ~~(acc.itemsCollector.reduce((acc, item) => acc + item[1], 0) / acc.itemsCollector.length * 100) / 100]],
        itemsCollector: []
      }
    }
    acc.counter++;
    return acc
  }, {counter: 0, data: [], itemsCollector: []}).data
  return efwefgwe
  // return [[start - 1, null], ...efwefgwe, [end + 1, null]]
}

module.exports = function (app) {
  /**
   * @description
   * Middleware for POST requests.
   * Parsing JSON body from request.
   */
  app.use(express.json());

  /**
   * @description
   * Middleware for logs requests
   */
  app.use(expressLogger);

  /**
   * @description
   * Middleware for receiving a client token from the UAA service,
   * and adding the received client token to the request headers.
   * Client token is needed to communicate with other backend services.
   */
  app.use('/', addClientToken);

  /**
   * @description
   * Middleware for verification of the user token, which adds AppHub to the request headers.
   * It can be turned off by a environment variable: USE_USER_TOKEN_CHECK.
   */

  const testData = [
    0,
    1,
    2,
    3,
    4,
    5
  ]

  app.use(
    '/cases/:id',
    (req, res) => {
      const data = testData[req.params.id];
      res.send({data})
    }
  );

  app.use(
    '/cases',
    (req, res) => {
      const data = testData;
      res.send({data})
    }
  );

  app.use(
    '/tags',
    (req, res) => {
      const data = tags;
      res.send({data})
    }
  );

  app.use(
    '/units',
    (req, res) => {
      console.log(req.body);
      const {tags} = req.body;
      const data = tags.reduce((acc, tag) => ({...acc, [tag]: units[tag]}), {});
      res.send({data})
    }
  );

  app.use(
    '/timeseries-api/livedata',
    proxy(`${TIMESERIES_BACKEND}`, {
      ...requestProxy,
      ...timeSeriesLivedataResDecorator
    })
  );
  app.use(
    '/timeseries-api/historical',
    (req, res) => {
      const {start, end, tags, aggregation, key} = req.body;
      const dataSet = tags.reduce((acc, tag) => {
        return {
          ...acc,
          [tag.tagName]: getData(start, end, aggregation, key)
        }
      }, {})
      res.send({data: dataSet})
    }
  );


};
