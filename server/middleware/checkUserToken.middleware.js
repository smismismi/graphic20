const predixFastToken = require('predix-fast-token');
const {
  UAA_AUTH_URL,
  APP_SCOPE,
  USE_USER_TOKEN_CHECK,
  IS_DEV,
  USER_TOKEN
} = require('../env');
const sendUnauthorized = require('../utils/sendUnauthorized');

module.exports = async (request, response, next) => {
  if (!USE_USER_TOKEN_CHECK) {
    return next();
  }

  const authHeader = request.headers && request.headers.authorization;
  const token = IS_DEV ? USER_TOKEN : authHeader && authHeader.split(' ')[1];

  if (!token) {
    sendUnauthorized(response, 'Not found user token in headers');
  }

  try {
    const verifiedToken = await predixFastToken.verify(token, [
      IS_DEV
        ? 'https://uaa-scpdr-uat-uat-r1.predix-uaa.run.ppc-jv-dev.sibintek.ru/oauth/token'
        : UAA_AUTH_URL
    ]);

    if (!verifiedToken.scope || !verifiedToken.scope.includes(APP_SCOPE)) {
      sendUnauthorized(
        response,
        'UAA application scope not found in user token'
      );
    } else {
      request.headers['user-name'] = verifiedToken.user_name;
      response.setHeader('user-name', verifiedToken.user_name);
      response.setHeader('user-id', verifiedToken.user_id);
      next();
    }
  } catch (error) {
    sendUnauthorized(response, error);
  }
};
