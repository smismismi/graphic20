const sendUnauthorized = require('../utils/sendUnauthorized');
const {
  CLIENT_TOKEN,
} = require('../env');

module.exports = async (request, response, next) => {
  request.setTimeout(5 * 60 * 1000);
  try {
    console.log(CLIENT_TOKEN);
    request.headers.authorization = `Bearer ${CLIENT_TOKEN}`;
    next();
  } catch (error) {
    sendUnauthorized(response, `Get UAA client token Error, ${error}`);
  }
};
