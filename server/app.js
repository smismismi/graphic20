'use strict';

const { PORT } = require('./env');
const express = require('express');
const path = require('path');
const app = express();
const proxies = require('./proxies');
const staticServerPort = PORT;

/**
 * @description
 * Static server.
 * Start static server for deployment production app build on production server.
 */
app.use(express.static(path.join(__dirname, 'app')));

/**
 * @description
 * Security middleware.
 * Check existing of token and return 403 if not exist.
 */

app.get('/', request => {
  request.sendFile(path.join(__dirname, 'app', 'index.html'));
});

/**
 * @description
 * Proxy handler.
 * The function contains all the work with proxies on
 * Development Node and Production Node.
 * Setup proxy settings for Development Node are located at "src/setupProxy.js".
 */
proxies(app);

app.listen(staticServerPort, () => {
  console.log(`Server running at ${staticServerPort} port`);
});
