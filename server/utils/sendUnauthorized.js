const { logger } = require('./logger');

module.exports = function sendUnauthorized(response, error) {
  logger.error(error);
  response.status(401).send(`Node Error: ${error}`);
};
