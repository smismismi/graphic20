const requestProxy = {
  proxyReqPathResolver(req) {
    console.log('+=======?', req.originalUrl);
    console.log(req.headers.authorization);
    return req.originalUrl;
  },
  userResDecorator(proxyRes, proxyResData, userReq) {
    if (userReq.method === 'DELETE') {
      const data = proxyRes.toString('utf8');
      return { data };
    }
    const data = JSON.parse(proxyResData.toString('utf8'));
    return { data };
  }
};

const timeSeriesLivedataResDecorator = {
  userResDecorator(proxyRes, proxyResData) {
    const data = proxyResData.toString('utf8');
    return {data};
  }
}

module.exports = {
  requestProxy,
  timeSeriesLivedataResDecorator
};
