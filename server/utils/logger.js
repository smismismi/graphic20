const pino = require('pino');
const logger = pino({
  prettyPrint: {
    translateTime: true
  }
});
const expressLogger = require('express-pino-logger')({ logger });

module.exports = {
  logger,
  expressLogger
};
